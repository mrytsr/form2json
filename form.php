<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<?php if (isset($_GET['form'])) { ?>
    <meta charset="utf-8" />
<?php }else{ ?>
    <meta charset="gb2312" />
<?php }?>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<title>渴切v3.3</title>
<link rel="stylesheet" href="keqie/assets/css/cutter.css">
<link rel="stylesheet" href="keqie/css/addons.css">
<!--[if ie 6]>
<link rel="stylesheet" href="keqie/assets/css/ie6.css">
<![endif]-->
<!--[if lt IE 9]>
<script src="keqie/assets/js/html5.js"></script>
<![endif]-->
<script src="keqie/assets/js/jquery-1.3.2.min.js"></script>
<script src="keqie/assets/js/cutter.js"></script>
<style type="text/css" media="screen">
    table td{border-top:none;}
</style>
</head>
<?php
/** 
 * 浏览器友好的变量输出 
 * @param mixed $var 变量 
 * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串 
 * @param string $label 标签 默认为空 
 * @param boolean $strict 是否严谨 默认为true 
 * @return void|string 
 */ 
function dump($var, $echo=true, $label=null, $strict=true) { 
    $label = ($label === null) ? '' : rtrim($label) . ' '; 
    if (!$strict) { 
        if (ini_get('html_errors')) { 
            $output = print_r($var, true); 
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>'; 
        } else { 
            $output = $label . print_r($var, true); 
        } 
    } else { 
        ob_start(); 
        var_dump($var); 
        $output = ob_get_clean(); 
        if (!extension_loaded('xdebug')) { 
            $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output); 
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>'; 
        } 
    } 
    if ($echo) { 
        echo($output); 
        return null; 
    }else 
        return $output; 
} 

function argsFilter($v){
    $matches = array();
    if(is_string($v) && preg_match('/\[(.*)\]/', $v, $matches)){
        $arr = explode(',', $matches[1]);
	foreach($arr as $k => $v){
	    if(preg_match('/"(.*)"/', $v, $matches)){
    	        $arr[$k] = $matches[1];
	    }else{
	        $arr[$k] = intVal($v);
	    }
	}
        if($arr){
            return $arr;
        }
    }
    return $v;
}

if (isset($_GET['form'])) {
    $action = '';
    if(isset($_GET['raw_json'])){
        $args = json_decode($_GET['raw_json'], true);
        $action = $args['Action'];
    }else if (!empty($_GET['args'])) {
        $args = $_GET['args'];
        $action = $_GET['action'];
    }

    if($action < 3000 && $action > 1000){
        echo 'v0.php';
        include 'v0.php';
    }else{
        echo 'v1.php';
        include 'v1.php';
    }

    // 添加args过滤函数
    $args = array_map('argsFilter', $args);
    $urlinfo = parse_url($_GET['url']);
    var_dump($_GET['url']);
    $json_str =  form2json($_GET['url'], $action, $args);

    echo '<blockquote>';
    echo '<textarea cols=70 rows=10>' . json_encode($args) . '</textarea>';
    echo '</blockquote>';
    echo '<blockquote>';
    dump(json_decode($json_str));
    echo '</blockquote>';
    echo '<blockquote>';
    echo '<textarea cols=70 rows=10>' . $json_str . '</textarea>';
    echo '</blockquote>';
    exit();
}

$url = isset($_GET['url']) ? $_GET['url'] : '';
$url_arr = array(
    'http://192.168.1.225/tjx/php/pui/admin.php?s=' => 'iloong/tjx(225)',
    'http://192.168.1.225/iloong/pui/' => '测试(225)',
    'http://58.246.135.237:20180/iloong/pui/'=>'idc测试(58.246.135.237:20180)',
    'http://uifolder.coolauncher.com.cn/iloong/pui/' => '正式(uifolder.coolauncher.com.cn)',
);
?>
<html>
<script language="javascript" type="text/javascript">
function changeAction(action)
{
    window.location = '?action=' + action + '&url=' + J_url.value;
}
jQuery.fn.serializeObject = function() {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};

    $.each(arrayData, function() {
        var value;

        if (this.value != null) {
            value = this.value;
        } else {
            value = '';
        }

        if (objectData[this.name] != null) {
            if (!objectData[this.name].push) {
                objectData[this.name] = [objectData[this.name]];
            }

            objectData[this.name].push(value);
        } else {
            objectData[this.name] = value;
        }
    });

    return objectData;
};
</script>
    <body>

<textarea id=J_args_json name=args_json cols=52 rows=5>
    <?php echo isset($_GET['args_json']) ? $_GET['args_json'] : '' ?>
</textarea>
<span onclick="console.log($('#J_form').serializeArray())">to json</span>
<span> | </span>
<span onclick="">to form</span>
</br>
        <form id=J_form target=response_iframe>
            发送目标<select name=url id=J_url value=<?php echo $url;?>>
                <?php
$selected = '';
foreach ($url_arr as $key => $value) {
    if ($url ? $key == $url : (isset($_COOKIE['url']) ? $key == $_COOKIE['url'] : false)) {
        $selected = 'selected';
        setcookie('url', $url);
    } else {
        $selected = '';
    }
    echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
}
                ?>
            </select><br>
<?php
$prev_rows = array();
function parse_sdk($csv_file)
{
    $hfile = fopen($csv_file, 'r');
    $sdk_arr = array();
    $action_index = '';
    while ($row = fgetcsv($hfile)) {
        $prev_rows[] = $row;
        if ($row[0] == 'Action') {
            $action_index = trim(trim($row[2], '“'), '”');
            $prev_2 = $prev_rows[count($prev_rows) - 1 - 2][0];
            $prev_3 = $prev_rows[count($prev_rows) - 1 - 3][0];
            $action = !empty($prev_2) && !preg_match('/^客户端/', $prev_2) ? $prev_2 : $prev_3;
            $sdk_arr[$action_index] = array();
            $sdk_arr[$action_index]['[action]'] = $action;
            continue;
        }else if (empty($row[0])) {
            $action_index = '';
            continue;
        }else if(empty($action_index)){
            continue;
        }

        // default
        if (isset($row[4])) {
            $default = $row[4];
        } else {
            $default = null;
        }

        // parent
        if (!empty($row[1]) && !preg_match('/无/', $row[1])) {
            $parent = $row[1];
        }else{
            $parent = '';
        }

        // 参数
        $arr = array(
            '[arg]' => $row[3],
            '[default]' => $default,
        );

        if (empty($parent)) {
            $sdk_arr[$action_index][$row[0]] = $arr;
        } else {
            if(empty($sdk_arr[$action_index][$parent])){
                $sdk_arr[$action_index][$parent] = array();
            }
            $parent_i = 0;
            while(isset($sdk_arr[$action_index][$parent][$parent_i][$row[0]])){
                $parent_i++;
            }
            $sdk_arr[$action_index][$parent][$parent_i][$row[0]] = $arr;
        }
    }
    return $sdk_arr;
}

function parse_head($csv_file)
{
    $hfile = fopen($csv_file, 'r');
    $head_arr = array();
    while ($row = fgetcsv($hfile)) {
        if (isset($row[0]) && preg_match('/^h/', $row[0])) {
            $default = isset($row[4]) ? $row[4] : '';
            $head_arr[$row[0]] = array(
                '[arg]' => $row[3],
                '[default]' => $default,
            );
        }
    }
    return $head_arr;
}

$sdk_arr = parse_sdk('sdk.csv.txt');





$selected = '';
echo '操作<select name=action onchange="changeAction(this.value)">';
$get_action = '';
if (isset($_GET['action'])) {
    $get_action = $_GET['action'];
}

function getAction(){
    global $get_action;
    global $first_action;
    return $get_action ? $get_action : (isset($_COOKIE['action']) ? $_COOKIE['action'] : $first_action); 
}

define('RAW_JSON', '-1');
$first_action = null;
foreach ($sdk_arr as $key => $action) {
    if ($get_action ? $key == $get_action : (isset($_COOKIE['action']) ? $key == $_COOKIE['action'] : false)) {
        $selected = 'selected';
        setcookie('action', $key);
    }else{
        $selected = '';
    }

    if (!$first_action) {
        $first_action = $key;
    }

    echo '<option value=' . $key . ' ' . $selected . '>' . $key . '.' . $action['[action]'] . '</optio>';
}

echo '<option value=' . RAW_JSON . ' ' . ($get_action == RAW_JSON ? 'selected' : '') . '>raw json</optio>';

echo '</select><br>';


echo '<input type=submit><br>';
echo '<hr>';

/* if (!empty($_GET['action'])) { */
/*     $action = $_GET['action']; */
/* } elseif (isset($first_action)) { */
/*     $action = $first_action; */
/* } */

$action = getAction();

if($action == RAW_JSON){
    show_raw_json();
}else{
    if($action > 1000 && $action < 3000){
        show_form($action, array(), $sdk_arr);
    }else{
        show_form($action, parse_head('sdk_head.csv'), $sdk_arr);
    }
}

function show_raw_json(){
?>
<textarea name=raw_json cols=52 rows=18>
    <?php echo isset($_GET['raw_json']) ? $_GET['raw_json'] : '' ?>
</textarea>
<?php
}

function show_form($action, $head_arr, $sdk_arr){
    // 输出head
    echo '<table>';
    foreach ($head_arr as $key => $arg) {
        echo '<tr>';
        // tjx 临时办法解决microentrance
        if($key == 'h13' && $action == 3200){
            $arg['[default]'] = 'microentrance';
    }
    echo '<td>' . $key . '.' . $arg['[arg]'] . '</td><td><input name=args[' . $key . '] value=' . $arg['[default]'] . '></td>';
    echo '</tr>';
    }
    echo '</table><hr>';


    echo '<table class="table noborder">';
    if (!empty($action)) {
        foreach ($sdk_arr[$action] as $key => $arg) {
            echo '<tr>';
            if ($key == '[action]') {
                continue;
        }

        if (isset($arg[0])) {
            // 如果是array
            foreach ($arg as $son_key => $son_arg) {
                foreach ($son_arg as $ke => $ar) {
                    echo '<tr>';
                    echo '<td>' . $ke . '.' . $ar['[arg]'] . '</td><td><input name=args[' . $key . '][' . $son_key . '][' . $ke . '] value=' . $ar['[default]'] . '></td>';
                    echo '<tr>';
                }
            }
        } else {
            echo '<td>' . $key . '.' . $arg['[arg]'] . '</td><td><input name=args[' . $key . '] value=' . $arg['[default]'] . '></td>';
        }
        echo '</tr>';
        }
    }
    echo '</table>';
}

?>
            <input type=hidden name=form value=you><br>
            <input type=submit><br>
        </form>
    </body>
</html>
