<?php

$md5key = 'f24657aafcb842b185c98a9d3d7c6f4725f6cc4597c3a4d531c70631f7c7210fd7afd2f8287814f3dfa662ad82d1b02268104e8ab3b2baee13fab062b3d27bff';

function form2json($url, $action, $args){
    $url .= 'ServicesEngine/DataService';
    function md5json($json) {
        global $md5key;
        $ser_json = json_encode($json, JSON_UNESCAPED_UNICODE);
        $key = $md5key;
        $ser_md5 = md5($ser_json . $key);
        $json ['md5'] = $ser_md5;
        return json_encode($json, JSON_UNESCAPED_UNICODE);
    }

    function ex_json_decode($str) {
        if (preg_match('/\w:/', $str)) {
            $str = preg_replace('/(\w+):/is', '"$1":', $str);
        }
        return $str;
    }

    function HTTP_Post($URL, $data) {
        $URL_Info = parse_url($URL);
        if (!isset($URL_Info["port"]))
            $URL_Info["port"] = 80;
        $path = $URL_Info["path"];
        if(isset($URL_Info['query'])){
            $path .= '?' . $URL_Info['query'];
        }
        $request = "POST " . $path . " HTTP/1.1\n";
        $request.="Host: " . $URL_Info["host"] . "\n";
        $request.="Content-type: application/x-www-form-urlencoded\n";
        $request.="Content-length: " . strlen($data) . "\n";
        $request.="Connection: close\n";
        $request.="\n";
        $request.=$data . "\n";
        $fp = fsockopen($URL_Info["host"], $URL_Info["port"]);
        fputs($fp, $request);
        $result = '';
        while (!feof($fp)) {
            $result .= fgets($fp, 1024);
        }
        fclose($fp);
        return $result;
    }

    date_default_timezone_set('Asia/Shanghai');
    $date = new DateTime();
    $arr_json = $args;
    $arr_json['Action'] = $action;
    $json = md5json($arr_json);

    $retdata = HTTP_Post($url, $json);
    $ret = substr($retdata, stripos($retdata,'{'));

    if(!$ret){
        return $retdata;
    }else{
        return $ret;
    }
}
