<?php
$md5key ='f24657aafcb842b185c98a9d3d7c6f4725f6cc4597c3a4d531c70631f7c7210fd7afd2f8287814f3dfa662ad82d1b02268104e8ab3b2baee13fab062b3d27bff';

function form2json($url, $action, $args){
    $url .= 'ServicesEngineV1/DataService';
    var_dump($url);
    /* 
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    function md5json($json) {
        global $md5key;
        $ser_json = json_encode($json, JSON_UNESCAPED_UNICODE);
        $key = $md5key;
        $ser_md5 = md5($ser_json . $key);
        $json['md5'] = $ser_md5;
        return json_encode($json, JSON_UNESCAPED_UNICODE);
    }


    function HTTP_Post($URL, $data)
    {
        $URL_Info=parse_url($URL);
        if(!isset($URL_Info["port"])){
            $URL_Info["port"]=80;
        }
        $path = $URL_Info["path"];
        if(isset($URL_Info['query'])){
            $path .= '?' . $URL_Info['query'];
        }
        $request="POST ". $path." HTTP/1.1\n";
        $request.="Host: ".$URL_Info["host"]."\n";
        $request.="Content-type: application/x-www-form-urlencoded\n";
        $request.="Content-length: ".strlen($data)."\n";
        $request.="Connection: close\n";
        $request.="\n";
        $request.=$data."\n";
        $fp = fsockopen($URL_Info["host"],$URL_Info["port"]);
        fputs($fp, $request);
        $result = '';
        while(!feof($fp)) {
            $result .= fgets($fp, 1024);
        }
        fclose($fp);
        return $result;
    }


    function GenClientPackV1($body) {
        global $md5key;
        $headlen = 32;
        $encode = 0;
        //对包体混淆加密
        $body = Translate($body);
        $encode = 1;

        $head = 'COE';      //标识
        $head .= pack('n', $headlen);  //包头长度
        $head .= pack('C', 1);  //协议版本号
        if (strlen($body) > 128) {//压缩协议
            $head .= pack('C', 1);
            $body = gzencode($body);
        } else {
            $head .= pack('C', 0);
        }
        $bodylen = strlen($body);
        //加密标识
        $head .= pack('C', $encode);
        //序号
        $head .= pack('N', 1);

        //MD5校验码
        $md5head = $head;
        $md5head .= pack('c16', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        $md5head .= pack('N', $bodylen);
        $md5data = $md5head . $body;

        $checkmd5 = md5($md5data . $md5key, true);
        $head .= $checkmd5;

        //包体长度
        $bodylen = strlen($body);
        $head .= pack('N', $bodylen);
        $data = $head . $body;
        return $data;
    }

    function Translate($unstring) {
        $ClientTable = array(22,-21,-32,-24,101,111,105,114,10,5,72,-76,-30,17,-109,-40,27,20,-126,-8,-46,-113,118,31,49,78,-18,95,-6,69,120,62,51,-119,113,-58,26,-101,-22,64,91,-56,-100,6,-42,-85,29,56,24,-39,16,-37,1,82,-120,-115,-9,-4,121,63,9,116,-5,-77,-59,-15,-13,-25,-111,48,97,4,32,19,-121,119,30,-43,-29,-118,-83,104,94,-74,110,-50,45,-45,-128,-75,55,-99,41,60,44,-78,-52,-92,52,-96,70,98,33,-41,-66,-91,107,109,61,37,-72,40,122,35,74,54,87,-107,-124,-68,65,80,-112,18,96,67,-14,-87,-16,-12,-57,-89,-63,3,-10,42,50,73,108,124,99,43,-26,-67,-2,-20,38,76,-44,21,-7,-31,117,2,66,46,25,-97,34,93,-71,-108,13,-36,92,-70,15,-69,106,36,88,123,-93,-79,-103,-51,-48,-94,77,-81,-17,79,0,103,-55,-19,-73,23,-49,83,-61,-102,-106,-95,7,-123,-127,-125,-35,90,-104,71,-84,-117,102,-105,-54,58,11,47,39,-23,127,-65,14,84,-80,-3,8,112,100,-33,-53,-122,-27,28,-110,68,59,12,-62,125,85,-64,89,-34,86,-90,-88,-28,57,-98,-11,53,126,115,-60,-82,-116,-38,-114,81,-86,-47,-1,75);    
        $len = strlen($unstring);
        for ($i = 0; $i < $len; $i++) {
            $d = ord($unstring[$i]);
            if ($d < 0) {
                $d += 256;
            }
            $unstring[$i] = chr($ClientTable[$d]);
        }
        return $unstring;
    }
    function DataProtocolAnalyseV1($data) {
        $json='';
        $headlen = unpack('n', substr($data, 3, 2))[1];
        $version = unpack('C', substr($data, 5, 1))[1];
        if ($version == 1) {
            $compress = unpack('C', substr($data, 6, 1))[1];
            $encrypt = unpack('C', substr($data, 7, 1))[1];
            $bodylen = unpack('N', substr($data, 8, 4))[1];
            if (strlen($data) != ($headlen + $bodylen)) {
                echo "CONTENT LEN FAIL\n";
                die();
            }
            $compressdata = substr($data, $headlen, $bodylen);
            if ($compress == 1) {
                $uncompressdata = gzdecode($compressdata);
            } else {
                $uncompressdata = $compressdata;
            }
            if ($encrypt == 0) {
                $json = $uncompressdata;
            } else if ($encrypt == 1) {
                $json = Translate($uncompressdata);
            } else if ($encrypt == 2) {
                $json = DES::cbc_decrypt($this->deskey, $uncompressdata);
            } else if ($encrypt == 3) {
                //RSA不支持
            }
        }
        return $json;
    }


    /* $arr_json['Action'] = '3000'; */
    $arr_json['Action'] = $action;
    $arr_json['h01'] = 'com.coco.jscalltest';
    $arr_json['h02'] = '11971';
    $arr_json['h03'] = '2.0.11971';
    $arr_json['h04'] = '3.1.11966.1';  //opversion
    $arr_json['h05'] = 'sh00184';  // sn
    $arr_json['h06'] = 'f1';       //appid
    $arr_json['h07'] = 'R001_TESTAFAE'; //shellid  R001_TESTAFAE  R001_TESTAFAE
    $arr_json['h08'] = '461026170103993';
    $arr_json['h09'] = '898600670912';
    $arr_json['h10'] = '354391053430810';
    $arr_json['h11'] = '15000000000';   // phone
/*
//* Action 3001    合并上传
    $arr_json['list']=array(
        array(
            'Action'=>'3101',
            'h07'=>'list_7',
            'h08'=>'18'        
        ),
        array(
            'Action'=>'3102',
            'h07'=>'list_8',
            'h08'=>'1'        
        )    
    );
 */

    $arr_json['h12'] = '4';
    $arr_json['h13'] = 'dynamicentrance';  //microentrance    dynamicentrance
    $arr_json['h14'] = 'ZTE U930';
    $arr_json['h15'] = 'ZTE U930V1.0.0B03';
    $arr_json['h16'] = '0';
    $arr_json['h17'] = '0';
    $arr_json['h18'] = '6263';
    $arr_json['h19'] = '2563';
    $arr_json['h20'] = '+8613010314500';
    $arr_json['h21'] = '854*480';
    $arr_json['h22'] = 'baima_ics2';
    $arr_json['h23'] = 'baima_ics2';
    $arr_json['h24'] = 'baima_ics2';
    $arr_json['h25'] = 'baima';
    $arr_json['h26'] = 'alps';
    $arr_json['h27'] = '4.0.4';
    $arr_json['h28'] = '15';
    $arr_json['h29'] = '481732';
    $arr_json['h30'] = '65360';
    $arr_json['h31'] = '515032';
    $arr_json['h32'] = '245196';
    $arr_json['h33'] = 'pisces';
    $arr_json['p1'] = '1';
    $arr_json['p2'] = 'cn.chelper';   //com.happyelements.androidbubble
    $arr_json['p3'] = '111110';  //drawable-hdpi  drawable
    $arr_json['p4'] = '00000000000000';   //配置时间戳
    $arr_json['p5'] = '00000000000000';   //列表时间戳
    $arr_json['p6'] = '0';

/*
//测试产品更新
    $arr_json['Array']= array(
        array(
            "p1"=>"com.cooee.Mylauncher",
            "p2"=>"0",         //versioncode
            "p3"=>"2.0.11971",
            "p4"=>"sh00184",    //sn
            "p5"=>"1",         // appid
            "p6"=>"1",
        ),
        array(
            "p1"=>"com.happyelements.androidbubble",
            "p2"=>"0",
            "p3"=>"2.0.11971",
            "p4"=>"shhc2870", //sn
            "p5"=>"f1",       //appid   
            "p6"=>"1",
        ),
        array(
            "p1"=>"cn.chelper1",
            "p2"=>"0",
            "p3"=>"2.0.11971",
            "p4"=>"sh00184",
            "p5"=>"f1", //
            "p6"=>"0",
        )
    );
 */


    //$url = 'http://192.168.1.225/iloong/pui/LogEngineV1/DataService';
    /* $url = 'http://192.168.1.225/iloong/pui/ServicesEngineV1/DataService'; */
    //$url = 'http://uifolder.coolauncher.com.cn/iloong/pui/LogEngineV1/DataService';
    //$url = 'http://uifolder.coolauncher.com.cn/iloong/pui/ServicesEngineV1/DataService';
    //$url = 'http://58.246.135.237:20180/iloong/pui/LogEngineV1/DataService';
    //$url = 'http://58.246.135.237:20180/iloong/pui/ServicesEngineV1/DataService';


    $args['Action'] = $action;

    $json=  md5json($args); 

    //echo $json;

    //$json='{"h11":"","p6":1,"h12":4,"p5":"00000000000000","h13":"dynamicentrance","p4":"00000000000000","h14":"baima_ics2","p3":300,"h33":"mt6575","h15":"ALPS.ICS2.MP.V1","p2":200,"h16":"WIFI","p1":100,"h17":"","h18":"","h30":162468,"h31":515032,"h32":444664,"h10":"","Action":"3300","h19":"","h24":"baima_ics2","h25":"baima","h22":"baima_ics2","h23":"baima_ics2","h28":15,"h29":481732,"h26":"alps","h27":"4.0.4","h07":"R001_TESTAFAE","h06":"f1","h05":"sh00184","h04":"1.0.0.0","h20":"","h03":"1.0","h21":"480*854","h02":1,"h01":"com.coco.jscalltest","h08":"","h09":""}';

    //$json='{"h11":"","h12":1,"h13":"com.cool.launcher","h14":"UMO T9300","h33":"sc8825","h15":"H500_A1_YM_8_W13.08.06_V0.4.4_20130503","p2":"com.pip.android.ss","h16":"WIFI","p1":1,"h17":"","h18":"43060","h30":145364,"h31":968284,"h32":510040,"h10":"359930012396888","Action":"3000","h19":"25362","h24":"sp8825ea","h25":"Spreadtrum","h22":"sp8825eaplus","h23":"sp8825ea","h28":15,"h29":434548,"h26":"Spreadtrum","h27":"4.2","h07":"R001_SHELLTST","h06":"f1","h05":"sh00184","h04":"1.0.0.0","h20":"","h03":"2.2.2.21128.140328","h21":"480*854","h02":21128,"h01":"com.cool.launcher","h08":"460023021348711","h09":""}';

    //echo $json;

    $data = GenClientPackV1($json);
    $retdata = HTTP_Post($url, $data);

    $coedata=substr($retdata,stripos($retdata,'COE'));

    $ret = DataProtocolAnalyseV1($coedata);
    if(!$ret){
        return $retdata;
    }else{
        return $ret;
    }
}
